﻿using System;
using System.Collections.Generic;

public static class Pangram
{
    public static bool IsPangram(string input)
    {
        HashSet<char> validLowerCaseCharSet = new HashSet<char>();
        foreach (var c in "abcdefghijklmnopqrstuvwxyz".ToCharArray() )
        {
            validLowerCaseCharSet.Add(c);
        }
        bool result = input.Length > 0;
        HashSet<char> readedCharSet = new HashSet<char>();
        foreach (var item in input.ToCharArray())
        {
            if ( (Char.IsLower(item) && readedCharSet.Contains(Char.ToUpper(item))) || 
                 (Char.IsUpper(item) && readedCharSet.Contains(Char.ToLower(item)))
               )
            {
                return false;
            }
            if (validLowerCaseCharSet.Contains(Char.ToLower(item)))
            {
                readedCharSet.Add(item);
            }
            
        }
        return validLowerCaseCharSet.Count == readedCharSet.Count;
    }
}
