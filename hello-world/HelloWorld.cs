﻿using System;

public static class HelloWorld
{
    public static string Hello()
    {
        string val = "World";
        return $"Hello, {val}!";
    }
}
